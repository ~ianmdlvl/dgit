-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: ruby-rails-3.2
Binary: ruby-rails-3.2, rails3
Architecture: all
Version: 3.2.6-1
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Antonio Terceiro <terceiro@debian.org>
Dm-Upload-Allowed: yes
Homepage: http://www.rubyonrails.org
Standards-Version: 3.9.3
Vcs-Browser: http://git.debian.org/?p=pkg-ruby-extras/ruby-rails.git;a=summary
Vcs-Git: git://git.debian.org/pkg-ruby-extras/ruby-rails-3.2.git
Build-Depends: debhelper (>= 7.0.50~), gem2deb (>= 0.3.0~)
Package-List: 
 rails3 deb ruby optional
 ruby-rails-3.2 deb ruby optional
Checksums-Sha1: 
 f36c3866b22de8ff6875fdbbfbcfb8d18e1f5a89 953 ruby-rails-3.2_3.2.6.orig.tar.gz
 7208250afe7083e258d1fa36cc3a60527608df11 2297 ruby-rails-3.2_3.2.6-1.debian.tar.gz
Checksums-Sha256: 
 207cfb1ef70aa9458c776deeda8e38ac977cbc852209828793b27d55bebc7bea 953 ruby-rails-3.2_3.2.6.orig.tar.gz
 55decdcdc8248a4153fb7e5688ffdc3c3a2661a95f3870edba3e1eaf40907088 2297 ruby-rails-3.2_3.2.6-1.debian.tar.gz
Files: 
 05a3954762c2a2101a10dd2efddf7000 953 ruby-rails-3.2_3.2.6.orig.tar.gz
 87bdb28ef5053d825bda80e959e2fd1c 2297 ruby-rails-3.2_3.2.6-1.debian.tar.gz
Ruby-Versions: all

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iEYEARECAAYFAk/nrgIACgkQDOM8kQ+cso9TjgCfcDl8MvUtKVZP6bPP9IrO93hP
TnAAn1aA67N088u6u/S2VA8UhYjNXhpO
=7sbS
-----END PGP SIGNATURE-----
